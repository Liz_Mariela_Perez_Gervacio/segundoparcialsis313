import Image from "next/image";

type ImagenesProps = {
    imagen: any,
}
const Imagenes= (props:ImagenesProps) => {
    const {imagen} = props;
    return (
        <div >
            <Image src={imagen} alt="" width={100} height={100}/>
            
        </div>
    )
}
export default Imagenes