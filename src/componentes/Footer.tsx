import Imagenes from "./Imagenes";

type FooterProps = {
    icono: any;
    title: any;
    text: any;
}
const Footer = (props: FooterProps) => {
    const { icono, title, text } = props;
    return (
        <div className="footer">
            <div className="footer1">
                <Imagenes imagen={icono}  />
            </div>
            <div className="footer2">
                <h5>{title}</h5>
                <a>{text}</a>
            </div>
        </div>
    )
}
export default Footer