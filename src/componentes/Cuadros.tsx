import Imagenes from "./Imagenes";

type CuadrosProps = {
    imagen: any;
    title: any;
    parafo: any;
}
const Cuadros = (props: CuadrosProps) => {
    const { imagen, title, parafo } = props;
    return (
        <div className="icono">
            <div className="iconos">
                <Imagenes imagen={imagen}  />
            </div>
            <div className="letras">
                <h4>{title}</h4>
                <p >{parafo}</p>
            </div>
        </div>
    )
}
export default Cuadros