import Imagenes from "./Imagenes";

type HeaderProps = {
    text: any,
    logo: any,
}
const Header = (props: HeaderProps) => {
    const { text, logo } = props;
    return (
        <div className="encabezado">
            <Imagenes imagen={logo}  />
            <p>{text}</p>
            <div >
            <ul className="lista">
                <li>About us</li>
                <li>Services</li>
                <li>Case Studies</li>
                <li>Blog</li>
                <li>How it Works</li>
                <li>Hire</li>
            </ul>
            </div>
        </div>
    )
}
export default Header