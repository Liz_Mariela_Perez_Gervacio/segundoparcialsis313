import Imagenes from "./Imagenes";

type ComentarioProps = {
    icono: any;
    title: any;
    parafo: any;
}
const Comentario= (props: ComentarioProps) => {
    const { icono, title, parafo } = props;
    return (
        <div className="hero">
            <div className="hero1">
                <Imagenes imagen={icono}  />
            </div>
            <div className="hero2">
                <h4>{title}</h4>
                <p >{parafo}</p>
            </div>
        </div>
    )
}
export default Comentario