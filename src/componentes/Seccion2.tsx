type Seccion2Props = {
    title: any,
    parafo: any,

}
const Seccion2 = (props: Seccion2Props) => {
    const { title, parafo } = props;
    return (
        <div >
            <div>
                <h3>{title}</h3>
                <p>{parafo}</p>
            </div>

        </div>
    )
}
export default Seccion2
