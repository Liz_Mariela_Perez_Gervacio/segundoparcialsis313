type TitlesProps = {
    title: any,
    parafo:any,

}
const  Titles= (props:TitlesProps) => {
    const {title,parafo } = props;
    return (
        <div className="pagsub">
            <h1>{title}</h1>
            <p>{parafo}</p>
        </div>
    )
}
export default Titles