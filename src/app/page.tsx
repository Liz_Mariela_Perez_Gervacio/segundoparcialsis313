import Image from "next/image";
import styles from "./page.module.css";
import Header from "@/componentes/Header";
import Button from "@/componentes/Button";
import Titles from "@/componentes/Titles";
import Imagenes from "@/componentes/Imagenes";
import Cuadros from "@/componentes/Cuadros";
import Seccion2 from "@/componentes/Seccion2";
import Comentario from "@/componentes/Comentario";
import Footer from "@/componentes/Footer";


export default function Home() {
  return (
    <main className={styles.main}>
      <div className="encabezado">
        <Header logo="/imagenes/logo.svg" text="lingh" ></Header>
        <Button text="Contact us"></Button>
      </div>
      <section>
        <div className="pag1">
          <div >
            <Titles title="Great Product is built by great teams" parafo="We help build and manage a team of world-class developers to bring your vision to life"></Titles>
            <Button text="Let’s get started!"></Button>
          </div>
          <div>
            <Imagenes imagen="/imagenes/imagen1.svg"></Imagenes>
          </div>
        </div>
      </section>
      <section >
        <div className="pag2">
          <div className="pagsub2">
            <Titles title="Helping a local business reinvent itself" parafo="We reached here with our hard work and dedication"></Titles>
          </div >
          <div className="padre">
            <Cuadros imagen="/imagenes/icono1.svg" title="2,245,341" parafo="Members"></Cuadros>
            <Cuadros imagen="/imagenes/icono2.svg" title="46,328" parafo="Clubs"></Cuadros>

            <Cuadros imagen="/imagenes/icono3.svg" title="46,328" parafo="Event Bookings"></Cuadros>
            <Cuadros imagen="/imagenes/icono4.svg" title="1,926,436" parafo="Payments"></Cuadros>
          </div>
        </div>
      </section>
      <div className="holwi">

        <Imagenes imagen="/imagenes/barra4.svg"></Imagenes>

      </div>
      <section>
        <div className="seccion2" >
          <div className="img">
            <Imagenes imagen="/imagenes/imagensec3.svg"></Imagenes>
          </div>
          <div className="letras">
            <Seccion2 title="Website Design for SCFC Canada" parafo="Born out of a vision, a single-minded objective that puts service before anything else, Swift Clearance and Forwarding Corp. surging forth to deliver the best services in the shipping and logistics scenario. Its meteoric rise stems out of a solid foundation. The management boasts of over 20 years of rich and varied experience in the shipping and freight forwarding industry."></Seccion2>
            <button>Real Mode</button>
          </div>

        </div>

      </section>
      <section>
        <div className="seccion3" >
          <div className="img">
            <Imagenes imagen="/imagenes/imagensec32.svg"></Imagenes>
          </div>
          <div className="letras">
            <Seccion2 title="Website Design for SCFC Canada" parafo="Born out of a vision, a single-minded objective that puts service before anything else, Swift Clearance and Forwarding Corp. surging forth to deliver the best services in the shipping and logistics scenario. Its meteoric rise stems out of a solid foundation. The management boasts of over 20 years of rich and varied experience in the shipping and freight forwarding industry."></Seccion2>
            <button>Real Mode</button>
          </div>
        </div>

      </section>

      <section>
        <div className="seccion4" >
          <div className="img">
            <Imagenes imagen="/imagenes/imagensec33.svg"/>
          </div>
          <div className="letras">
            <Seccion2 title="Website Design for SCFC Canada" parafo="Born out of a vision, a single-minded objective that puts service before anything else, Swift Clearance and Forwarding Corp. surging forth to deliver the best services in the shipping and logistics scenario. Its meteoric rise stems out of a solid foundation. The management boasts of over 20 years of rich and varied experience in the shipping and freight forwarding industry."/>
            <button>Real Mode </button>
          </div>
        </div>
      </section>
      <div className="titulodecomentario">
        <Imagenes imagen="/imagenes/titulo.svg"/>
      </div>
      <section className="comentarios">

        <div className="comentario1">
          <Comentario icono="/imagenes/avion.svg" title=" UX Driven Engineering" parafo="Unlike other companies, we are a UX first development company. Projects are driven by designers and they make sure design and experiences translate to code." />
          <Comentario icono="/imagenes/corazon.svg" title=" Proven Experience and Expertise" parafo="Unlike other companies, we are a UX first development company. Projects are driven by designers and they make sure design and experiences translate to code." />
          <Comentario icono="/imagenes/bien.svg" title=" Code Reviews" parafo="Unlike other companies, we are a UX first development company. Projects are driven by designers and they make sure design and experiences translate to code." />
        </div>
        <div className="comentario2">
          <Comentario icono="/imagenes/signo.svg" title=" Developing Shared Understanding" parafo="Unlike other companies, we are a UX first development company. Projects are driven by designers and they make sure design and experiences translate to code.." />
          <Comentario icono="/imagenes/verde.svg" title="Security & Intellectual Property (IP)" parafo="Unlike other companies, we are a UX first development company. Projects are driven by designers and they make sure design and experiences translate to code." />
          <Comentario icono="/imagenes/avion.svg" title=" Quality Assurance & Testing" parafo="Unlike other companies, we are a UX first development company. Projects are driven by designers and they make sure design and experiences translate to code" />
        </div>
        </section>
        <section>
          <div className="footer">
            <Footer icono="/imagenes/logo.svg" title="Ik developers" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry."/>
            <Footer icono="/imagenes/logo.svg" title="Ik developers" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry."/>
          </div>
        </section>
    </main >
  );
}
